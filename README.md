This is the presentation for the HDL coding methodology I used in my master's degree.

- to see it in english: [hdl-methodology](https://gitlab.com/viniciusglinden/hdl-methodology/-/raw/master/hdl-methodology.pdf)
- para ver em português: [metodologia-hdl](https://gitlab.com/viniciusglinden/hdl-methodology/-/raw/master/metodologia-hdl.pdf)

# Compiling

This presentation uses `bibtex` for references. To compile it manually, use:

1. Compile all images to PDF through Inkscape
2. Compile LaTeX: `pdflatex main.tex`
3. Compile bibliography: `bibtex main`
4. Compile LaTeX; and
5. Compile LaTeX;

Or, if you don't want to suffer, after having all the images converted:

```sh
latexmk main.tex
```

Table of Contents may not generate at first. You should then force-compile it
again.

