\input{config.tex}
\usepackage{listings} % include code

% manufacturer's specific attributes

\title{Method for Coding in\linebreak Hardware Description Language}
\author{Vinícius Gabriel Linden}
\institute{Unisinos University\linebreak Shared Knowledge Room (SKR)}
\date{August 27th 2020}

\lstset{
	language=VHDL,
	basicstyle=\tiny,
	tabsize=4
}

\begin{document}

\frame{\titlepage}

\begin{frame}{Table of Contents}
	\tableofcontents
\end{frame}

\section{Hardware Description \textit{Languages}}

\begin{frame}{Hardware Description \textit{Languages}}
	\begin{itemize}
		\item VHDL: the most strongly typed language\footnote{my language of
			choice}
		\item Verilog: strongly typed, but not as much as VHDL
		\item SystemVerilog: considered the evolution of Verilog
		\item SystemC: set of C++ classes and macros \underline{for event-driven
			simulation}
	\end{itemize}

	Different languages serve different purposes.

	Good simulators are mixed language simulators!
\end{frame}

\begin{frame}{Hardware Description \textit{Languages}}
	\begin{itemize}
		\item Remember what \textit{Hardware \underline{Description Language}}
			stands for!
		\item You are coding a circuit, based on the resources available by the
			FPGA
		\item You should consider different FPGAs
		\item You should consider implementation as an ASIC
	\end{itemize}
\end{frame}

\section{Coding Principles}
\begin{frame}{Principles}
	\begin{itemize}
		\item KISS Principle: things work better if they are \underline{designed
				as simple as possible} --- \textit{Keep It Simple, Stupid!}
		\item Inspiration from Software Architecture, but considering the
			differences between a \textit{hardware} and a \textit{software}
			languages
	\end{itemize}
\end{frame}

\begin{frame}{Principles}
	\cite{VHDLMethodology}, therefore:
	\begin{itemize}
		\item Modularity: project must be constructed via modules
		\item Flexibility: modules should have as common interfaces as
			possible\footnote{files should also have a common look}
		\item Portability: only use tools that are common among manufacturers
	\end{itemize}
\end{frame}

\section{Abstraction}
\begin{frame}{Abstraction}
	\begin{minipage}{.4\linewidth}
		\includegraphics[width=\linewidth]{HDL_files_hierarchy.pdf}
	\end{minipage}%
	\begin{minipage}{.6\linewidth}
		\begin{itemize}
			\item Simulation: stimulae for testing
			\item Test bench: complex simulation, normally containing some
				automation\footnotemark
			\item Top-level: adaptation of the top-most module to the current
				hardware
			\item (Sub-)module: functional code, which describes a specific
				logic
			\item Instantiation: adaptations from primitives to common
				interfaces
		\end{itemize}
	\end{minipage}
	\footnotetext{normally, test bench is considered the same as simulation}
\end{frame}

\section{Coding Style}
\begin{frame}{Coding Style}
	\begin{itemize}
		\item \underline{Synchronous is better} than asynchronous: timing
			constraints\footnote{Keep It Synchronous, Stupid (KISS)}
		\item Be careful with libraries: \underline{not all libraries are
			recommended} for synthesis
		\item Finite State Machines (FSMs) are your friend: they help you
			conceptualize, document and simplify implementation
		\item Use direct instantiation whenever possible\footnote{direct
			instantiation is used in the example, see the Additional Notes}
	\end{itemize}
\end{frame}

\begin{frame}{Coding Style}
	\begin{itemize}
		\item The \underline{order of the sensitivity} list changes the
			priorities
		\item Declare common things (e.g.: \texttt{constant}, \texttt{function},
			\texttt{procedure}) in \underline{code packages}
		\item Use only active-high signals, according to their names: an
			active-low busy port is an active-high available port\footnote{one
			should at least indicate that the signal is active-low in its name}
		\item Use parenthesis inside boolean equations only when
			necessary~\cite{VHDL_IEEE}
	\end{itemize}
\end{frame}

\begin{frame}{Coding Style}
	\begin{itemize}
		\item You should avoid introducing signals other than \texttt{clk} and
			\texttt{rst} in the sensitivity list: signal introduction to the
			\underline{sensitivity list} results in using the \underline{dedicated
				clock path}\footnote{always think about the hardware
				implementation and possible timing issues!}
	\end{itemize}
	\includegraphics[width=\linewidth]{FPGA-inside.pdf}
\end{frame}

\begin{frame}{VHDL Style}
	\begin{itemize}
		\item However, if no clock nor reset signal is listed, feel free to
			include other signals
		\item Only employ \texttt{std\_logic} and \texttt{std\_logic\_vector} for
			ports\footnote{anything can be used in the \texttt{generic}s}
		\item \underline{Don't use \texttt{variable}}, only \texttt{signal}:
			\texttt{variable}s can lead to problems and since each module is
			small, they are not necessary
	\end{itemize}
\end{frame}

\begin{frame}{VHDL Style}
	\begin{itemize}
		\item \texttt{assert} statements are nice for simulation: they help you
			validate \& debug by speeding up and removing human error
		\item Use \texttt{clk'event and clk = '1'} instead of
			\texttt{clk'rising\_edge}: \underline{portability \& control}
		\item Use \texttt{function} and \texttt{procedure} statements
			\underline{only for the ``preprocessor''}\footnote{e.g.
			\texttt{generic}s} and simulation: most of it is not synthesizable
	\end{itemize}
\end{frame}

\section{Simple Example}
\begin{frame}{Example --- Project Requirements}
	\begin{itemize}
		\item A FIFO adapted to the streaming standard below
			\begin{itemize}
				\item \texttt{valid}: sink asserts only in the first byte,
					signaling start of stream
				\item \texttt{ready}: source asserted only when needs to send a
					stream
				\item \texttt{N} is fixed since instantiation (through generics)
			\end{itemize}
		\item Ports for controlling package retainment and package unloading
	\end{itemize}
	\begin{minipage}{.6\linewidth}
		\begin{center}
			\includegraphics[width=\linewidth]{nvme_stream.pdf}
		\end{center}
	\end{minipage}%
	\begin{minipage}{.4\linewidth}
		\centering
		\includegraphics[width=\linewidth]{FIFO-NVMe-idea.pdf}
	\end{minipage}
\end{frame}

\begin{frame}{Example --- Proposed Architecture}
	\begin{itemize}
		\item ``write'' and ``read'' are FSMs with a counter
		\item ``delay'' may be implemented as a separate module
		\item ``FIFO'' may be implemented as a primitive instantiation
	\end{itemize}

	\includegraphics[width=\linewidth]{FIFO_NVMe.pdf}
\end{frame}

\begin{frame}{Example -- Development}
	\begin{minipage}{.5\linewidth}
		Just to quickly show that FSM\\
		designs can be very simple\footnotemark

		\begin{center}
			\includegraphics[width=.7\linewidth]{FIFO_NVMe_wr_fsm.pdf}

			Write (Moore) FSM
		\end{center}
	\end{minipage}%
	\begin{minipage}{.5\linewidth}
		\centering
		\includegraphics[width=.7\linewidth]{FIFO_NVMe_rd_fsm.pdf}

		Read (Mealy) FSM
	\end{minipage}
	\footnotetext{no convention is used for these drawings}
\end{frame}

\begin{frame}{Example --- Results}
	Checkout the codes!
	\begin{itemize}
		\item ``inst\_FIFO.vhd'': FIFO primitive instantiation
		\item ``memory\_pkg.vhd'': common functions for FIFO
		\item ``rtl\_counter.vhd'': counter with saturation
		\item ``rtl\_FIFO\_NVMe\_rd.vhd'': ``read'' FSM
		\item ``rtl\_FIFO\_NVMe\_wr.vhd'': ``write'' FSM
		\item ``rtl\_FIFO\_NVMe.vhd'': module assembly
		\item ``sim\_FIFO\_NVMe.vhd'': module assembly
			simulation\footnote{missing some good coding practices, can you
			guess what they are?}
	\end{itemize}
	For simulation, visit \url{gitlab.com/storagesystemslaboratory/nvme-switch}
\end{frame}

\section{Additional notes}
\begin{frame}{Additional Notes}
	\begin{itemize}
		\item One should keep good documentation practices
		\item Wavedrom\footnote{\url{wavedrom.com}} is fantastic for waveform
			drawings and even bitfield description
		\item Doxygen\footnote{Doxygen is a software for code documentation
			through code comments} is VHDL compatible, but I never used it as
			such
		\item Different manufacturers implement different coding environments
			(IDE)\footnote{I personally use neoViM with syntastic and GHDL for
			linting}
	\end{itemize}
\end{frame}

\begin{frame}{Additional Notes}
	\centering
	Automatic test bench example
	\includegraphics[width=\linewidth]{automatic-test-bench.pdf}
\end{frame}

\begin{frame}{Additional Notes}
	\centering
	HDL coding and implementation workflow
	\includegraphics[width=.6\linewidth]{HDL-implementation.pdf}
\end{frame}

\begin{frame}[fragile]{Additional Notes}
	\begin{center}
		VHDL explanation of instantiation types
	\end{center}
	\begin{minipage}{.5\linewidth}
		\begin{lstlisting}
-- DIRECT INSTANTIATION
architecture m_arch of m_arch is
begin
	m_label :
	entity m_library.m_component
	port map (
		-- port mapping
	);
end m_arch;


-- INDIRECT INSTANTIATION
architecture m_arch of m_arch is

	component m_component is
	port (
		-- port declaration
	);
	end component m_component;

begin
	m_label :
	component m_component
	port map (
		-- port mapping
	);
end m_arch;
		\end{lstlisting}
	\end{minipage}%
	\begin{minipage}{.5\linewidth}
		\begin{tiny}
			\begin{itemize}
				\item Direct: recommended for its simplicity, however impossible
					for IPs
				\item Indirect: has to keep "a local copy" of the component
					interface, therefore making IP usage possible
				\item Mixed Type: I don't know a use case for this one
			\end{itemize}
		\end{tiny}
		\begin{lstlisting}
-- MIXED TYPE INSTANTIATION
archirecture m_arch of m_arch is

	component m_component
	port (
		-- port declaration
	)
	end component;

	for component_name :
	component use
	entity m_library.component;

begin
	component_name :
	component port map (
		-- port mapping
	);
end m_arch;
		\end{lstlisting}
	\end{minipage}
\end{frame}

\section{Bibliography}
\begin{frame}{Bibliography}
	\nocite{*} % cite all the unreferenced bibliography
	\bibliographystyle{abbrv}
	\bibliography{library.bib}
\end{frame}

\section{Acknowledgments}
\begin{frame}{Acknowledgments}
	Special thanks to\\
	\begin{center}
		Dr. Rodrigo Marques de Figueiredo and\\
		Dr. Lúcio Rene Prade\\
	\end{center}
	for their contributions to my coding practices and guidance
\end{frame}

\end{document}
