--------------------------------------------------------------------------------
--
-- Module Name: rtl_FIFO_NVMe_wr
-- Create Date: 09/16/2019 (MM/DD/YYYY)
-- Revision: 1
--
-- Company: ittChip - Unisinos
-- Engineer: Vinícius Gabriel LINDEN
-- 
-- Description: Real-time logic for FIFO NVMe automatic load
-- 
--------------------------------------------------------------------------------
-- TODO:

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.NVMe_pkg.all;

entity rtl_FIFO_NVMe_wr is
	generic
	(
		SUBMISSION_CMD 		: boolean := true -- true: submission command; false: completion command
	);
	port
	(
		-- INPUT
		valid 				: in std_logic; -- sending new data stream
		ready 				: out std_logic; -- do not send new stream, module is busy

		-- CONTROL
		full 				: in std_logic;
		block_pkgs 			: in std_logic; -- stop receiving packets
		number_pkgs 		: out std_logic_vector(7 downto 0); -- number of packages stored

		-- FIFO
		counter_en 			: out std_logic; -- internal counter enable
		wrcount 			: in std_logic_vector(13 downto 0); -- FIFO's wrcount

		-- GENERAL PURPOSE
		error 				: out std_logic;
		rst 				: in std_logic;
		clk 				: in std_logic
	);
end rtl_FIFO_NVMe_wr;

architecture rtl of rtl_FIFO_NVMe_wr is

	constant PACKAGE_LENGTH : integer := nvme_cmd_size(SUBMISSION_CMD);

	type fsm_state is (IDLE, RECEIVING);
	signal state_fsm 		: fsm_state;

	signal ready_i 			: std_logic;
	signal saturated 		: std_logic;
	signal counter_en_i 	: std_logic;
	signal cnt_unsigned 	: unsigned(13 downto 0); -- number of packages
	signal error_nonstate 	: std_logic; -- undefined state

begin

	error <= error_nonstate;
	counter_en <= counter_en_i;
	ready <= ready_i;

	cnt_unsigned <= unsigned(wrcount) + 1 when wrcount /= "11111111111111";

	SUBMISSION : if SUBMISSION_CMD generate
		number_pkgs <= std_logic_vector(cnt_unsigned(13 downto 6));
	end generate SUBMISSION;

	COMPLETION : if not SUBMISSION_CMD generate
		number_pkgs <= std_logic_vector(cnt_unsigned(11 downto 4));
	end generate COMPLETION;

	COUNTER : entity work.rtl_counter
	generic map
	(
		CNT_RANGE => PACKAGE_LENGTH-1,
		CNT_RESET => 0,
		CNT_SATURATE_EN => false
	)
	port map
	(
		rst => rst,
		clk => clk,
		clk_en => '1',
		cnt_en => counter_en_i,
		cnt => open,
		sat => saturated
	);

	process(rst,clk)
	begin
		if rst = '1' then
			state_fsm <= IDLE;
			error_nonstate <= '0';
		elsif clk'event and clk = '1' then
			case state_fsm is
				when IDLE =>
					if valid = '1' and ready_i = '1' then
						state_fsm <= RECEIVING;
					end if;
				when RECEIVING =>
					if saturated = '1' then
						state_fsm <= IDLE;
					end if;
				when others =>
					state_fsm <= IDLE;
					error_nonstate <= '1';
			end case;
		end if;
	end process;
	ready_i <= (not block_pkgs) and (not full) when state_fsm = IDLE else '0';
	counter_en_i <= '1' when state_fsm = RECEIVING else '0';

end rtl;


